# -*- mode: ruby -*-
# vi: set ft=ruby :

# number of virtual maschines include ansible host
VM_COUNT = 5
# set ethernet card name
ETHERNET_CARD = "Сетевая карта Realtek RTL8101E Family PCI-E Fast Ethernet NIC (NDIS 6.20)"
#ETHERNET_CARD = "Killer E2400 Gigabit Ethernet Controller"
# network type for project ("private" or "public")
NETWORK_TYPE = "private"
# host names
VM_HOSTNAME = "230103-docker"
# "true" if you need ansible in this project
NEED_ANSIBLE = true
# "true" if you want to autoplay ansible playbook
ANSIBLE_AUTOPLAY = false
# type of ansible provision: "ansible" or "ansible_local"
ANSIBLE_TYPE = "ansible"
# ansible host name
ANSIBLE_HOSTNAME = "230103-ansible"
# initialize variable for ansible
ANSIBLE_HOSTS = []

if VM_COUNT <= 0
  abort "Incorrect number of virtual machines. Variable VM_COUNT must be greater than 0."
end
if VM_HOSTNAME == ""
  VM_HOSTNAME = "vm-"
end

def standard_vm_create(config, hostname, ip_address, script)
  config.vm.define hostname do |host|
    host.vm.provider "virtualbox" do |vb|
      vb.name = hostname
      vb.cpus = "2"
      vb.memory = "2048"
    end
    host.vm.hostname = hostname
    host.vm.network "#{NETWORK_TYPE}_network", bridge: ETHERNET_CARD, ip: ip_address
    if script != ""
      config.vm.provision "shell", path: script
    end
  end
end

Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"
  config.vm.box_check_update = false
  config.vm.boot_timeout = 900

  (1..VM_COUNT).each do |count|
    if count == VM_COUNT
      if NEED_ANSIBLE == true
        if ANSIBLE_HOSTNAME == ""
          ANSIBLE_HOSTNAME = "ansible"
        end
        standard_vm_create(config, ANSIBLE_HOSTNAME, "192.168.100.10", "ansible_script.sh")
        if ANSIBLE_AUTOPLAY == true
          ANSIBLE_HOSTS[VM_COUNT] = { :hostname => ANSIBLE_HOSTNAME, :ip_addr => "192.168.100.10" }
          ANSIBLE_HOSTS.each do |vm|
            config.vm.provision "#{ANSIBLE_TYPE}" do |ansible|
              ansible.limit = "all"
              ansible.playbook = "./ansible/playbook.yml"
              ansible.verbose = "-v"
              ansible.groups = {
                "ansibles" => ["#{ANSIBLE_HOSTNAME}"],
                "dockers" => ["#{VM_HOSTNAME}-[1:#{VM_COUNT}]"],
                "docker_swarm_managers" => ["#{VM_HOSTNAME}-1"],
                "docker_swarm_workers" => ["#{VM_HOSTNAME}-[2:#{VM_COUNT}]"],
                "docker_swarm_hosts:children" => ["ansibles", "dockers"],
              }
            end
          end
        end
      else
        standard_vm_create(config, "#{VM_HOSTNAME}-#{count}", "192.168.100.#{10+count}", "vm_script.sh")
      end
    else
      standard_vm_create(config, "#{VM_HOSTNAME}-#{count}", "192.168.100.#{10+count}", "vm_script.sh")
      if count == 1
        config.vm.define "#{VM_HOSTNAME}-1" do |host|
          host.vm.network  "forwarded_port", guest: 9000, host: 9000
          host.vm.network  "forwarded_port", guest: 9001, host: 9001
        end
      end
      if ANSIBLE_AUTOPLAY == true
        ANSIBLE_HOSTS[count - 1] = { :hostname => "#{VM_HOSTNAME}-#{count}", :ip_addr => "192.168.100.#{10+count}" }
      end
    end
  end    
end
