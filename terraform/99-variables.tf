# provider
variable "sa_key_file" {
  description = "name of service account key file"
  type = string
  default = "key.json"
}
variable "cloud_id" {
  description = "cloud id"
  type = string
  default = "b1gxxxxxxxxxxxxxxxxx"
}
variable "folder_id" {
  description = "folder id"
  type = string
  default = "b1gxxxxxxxxxxxxxxxxx"
}

# service
variable "sa_name" {
  description = "name of your service account"
  type = string
  default = "my-sa-account"
}
variable "bucket_minio_name" {
  description = "minio bucket name. bucket name is unique globally"
  type = string
  default = "bucket-minio-test1"
}
variable "bucket_sa_permissions" {
  description = "service account permissions for minio bucket: READ, WRITE, READ_ACP, WRITE_ACP"
  type = list(string)
  default = ["READ","WRITE"]
}
variable "yc_s3_memory_limit" {
  description = "memory limit for yandex cloud stroage class"
  type = number
  default = 1000
}

# networks
variable "cidr_subnet_a" {
  description = "subnet address"
  type = string
  default = "192.168.1.0/24"
}
variable "availibility_zone_a" {
  description = "availibility zone: ru-central1-a, ru-central1-b, ru-central1-c (will be disabled)"
  type = string
  default = "ru-central1-a"
}
variable "region_var" {
  description = "it is only one region. you don't need to change this variable"
  type = string
  default = "ru-central1"
}

# kubernetes
variable "cluster_name" {
  description = "name of kunernetes cluster"
  type = string
  default = "k8s-cluster"
}
variable "node_group_name" {
  description = "name of node group"
  type = string
  default = "nodegroup-1"
}
variable "instance_platform" {
  description = "platform for nodes instances: standard-(v1-v3), standard-v3-t4, gpu-standard-(v1-v3)"
  type = string
  default = "standard-v3"
}
variable "instance_core_fraction" {
  description = "cpu fraction for nodes instances"
  type = number
  default = 100
}
variable "instance_memory" {
  description = "memory for nodes instances"
  type = number
  default = 2
}
variable "instance_cpu_cores" {
  description = "cpu cores for nodes instances"
  type = number
  default = 2
}
variable "instance_disk_type" {
  description = "type of instance boot disk: network-hdd, network-ssd, network-ssd-nonreplicated"
  type = string
  default = "network-hdd"
}
variable "instance_disk_size" {
  description = "instance boot disk size (Gb). minimum 30"
  type = number
  default = 30
}
variable "instance_is_preemtible" {
  description = "set preemtible"
  type = bool
  default = false
}
variable "min_nodes" {
  description = "minimum nodes in cluster"
  type = number
  default = 1
}
variable "max_nodes" {
  description = "maximum nodes in cluster (can not be less than min_nodes)"
  type = number
  default = 3
}
variable "initial_nodes" {
  description = "number of nodes at start (can not be less than min_nodes)"
  type = number
  default = 1
}
variable "maitenance_day" {
  description = "days for maintenance: monday, tuesday, etc."
  type = string
  default = "sunday"
}
variable "maitenance_start_time" {
  description = "time for maintenance: hh:mm"
  type = string
  default = "5:00"
}
variable "maitenance_duration" {
  description = "maitenance duration time"
  type = string
  default = "1h"
}

# locals
variable "kubeconfig_file" {
  description = "path to kubernetes config file"
  type = string
  default = "/dev/k8s/config"
}
variable "locals_api_version" {
  description = "api version. it is just only one. can be changed in future"
  type = string
  default = "v1"
}
variable "locals_cluster_name" {
  description = "cluster name for locals"
  type = string
  default = "kubernetes"
}
variable "locals_user" {
  description = "user for locals"
  type = string
  default = "user"
}

# minio storage parameters
variable "storage_capacity" {
  description = "capacity of storage: number and letter designation. ex: 4Gi, 100M, etc."
  type = string
  default = "4Gi"
}
variable "minio_volume_name" {
  description = "minio volume name"
  type = string
  default = "minio-data"
}
variable "storage_access" {
  description = "access type: ReadWriteOnce, ReadOnlyMany or ReadWriteMany"
  type = list(string)
  default = ["ReadWriteOnce"]
}

# minio config
variable "minio_replicas_num" {
  description = "number of minio replicas"
  type = number
  default = 2
}
variable "minio_url" {
  description = "url of you minio service"
  type = string
  default = "http://example/url"
}
variable "minio_port" {
  description = "port where you minio service listen"
  type = string
  default = "9001"
}
variable "minio_volume_mount_path" {
  description = "mount path in storage for minio volume"
  type = string
  default = "/data"
}
