# create k8s cluster
resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name = var.cluster_name
  network_id = yandex_vpc_network.network-a.id
  master {
    zonal {
      zone      = yandex_vpc_subnet.subnet-a.zone
      subnet_id = yandex_vpc_subnet.subnet-a.id
    }
    public_ip = true
    security_group_ids = [yandex_vpc_security_group.secgroup.id]
    maintenance_policy {
      auto_upgrade = true
      maintenance_window {
        day        = var.maitenance_day
        start_time = var.maitenance_start_time
        duration   = var.maitenance_duration
      }
    }
  }
  service_account_id      = yandex_iam_service_account.sa.id
  node_service_account_id = yandex_iam_service_account.sa.id
    depends_on            = [
      yandex_resourcemanager_folder_iam_member.editor,
      yandex_resourcemanager_folder_iam_member.images-puller
    ]
  kms_provider {
    key_id = yandex_kms_symmetric_key.kms-key.id
  }
}

# create cluster nodes
resource "yandex_kubernetes_node_group" "nodegroup-1" {
  cluster_id  = yandex_kubernetes_cluster.k8s-cluster.id
  name        = var.node_group_name
  instance_template {
    platform_id  = var.instance_platform
    network_interface {
      nat        = true
      subnet_ids = [yandex_vpc_subnet.subnet-a.id]
    }
    resources {
      core_fraction = var.instance_core_fraction
      memory        = var.instance_memory
      cores         = var.instance_cpu_cores
    }
    boot_disk {
        type = var.instance_disk_type
        size = var.instance_disk_size
    }
    scheduling_policy {
      preemptible = var.instance_is_preemtible
    }
    container_runtime {
      type = "containerd"
    }
    metadata = {
      user-data = "${file("./meta.yml")}"
    }
  }
  scale_policy {
    auto_scale {
      min     = var.min_nodes
      max     = var.max_nodes
      initial = var.initial_nodes
    }
  }
  allocation_policy {
    location {
      zone = var.availibility_zone_a
    }
  }
}
