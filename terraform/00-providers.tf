terraform {
  required_version = ">= 0.13"
  required_providers {
    yandex   = {
      source = "yandex-cloud/yandex"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
    }
  }

# move terraform.tfstate in bucket
# keys moved in backend.conf. init terraform with --backend-config=backend.conf
  backend "s3" {
    endpoint                    = "storage.yandexcloud.net"
    bucket                      = "bucket-tfstate"
    region                      = "ru-central1"
    key                         = "terraform.tfstate"
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  service_account_key_file = var.sa_key_file
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.availibility_zone_1
}

provider "kubernetes" {
  host = format("https://%s", yandex_kubernetes_cluster.k8s-cluster.master.0.external_v4_address)
  config_path = "~/.kube/config"
}
