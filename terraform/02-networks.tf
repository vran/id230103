# create network
resource "yandex_vpc_network" "network-a" {
  name = "network-a"
  description = "network for k8s"
}

# create subnets
resource "yandex_vpc_subnet" "subnet-a" {
  name           = "subnet-a"
  zone           = var.availibility_zone_a
  network_id     = yandex_vpc_network.network-a.id
  v4_cidr_blocks = [var.cidr_subnet_a]
}
