resource "yandex_vpc_security_group" "secgroup" {
  name        = "secgroup"
  network_id  = yandex_vpc_network.network-a.id

# allow internal traffic
  ingress {
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

# open input-ports from list
  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = var.minio_port
    to_port        = var.minio_port
  }

# open output-ports
  egress {
    protocol       = "TCP"
    to_port           = 65535
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}
