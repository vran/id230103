# create service account and add permissions
resource "yandex_iam_service_account" "sa" {
  name = var.sa_name
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
}

# create bucket
resource "yandex_storage_bucket" "bucket-minio" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = var.bucket_minio_name
}

# create kms-key
resource "yandex_kms_symmetric_key" "kms-key" {
  name              = "kms-key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}

# create stroage class for yandex cloud buckets
resource "kubernetes_storage_class" "yc-s3" {
  metadata {
    name = "yc-s3"
  }
  storage_provisioner = "ru.yandex.s3.csi"
  parameters = {
    mounter = "geesefs"
    options = "--memory-limit=${var.yc_s3_memory_limit} --dir-mode=0777 --file-mode=0666 --uid=1001"
    bucket  = var.bucket_minio_name
  }
}
