# create volume for minio. it uses s3 bucket and storage class from the file "01-service.tf"
resource "kubernetes_persistent_volume" "minio_pv" {
  metadata {
    name = "minio-pv"
  }
  spec {
    capacity  = {
      storage = var.storage_capacity
    }
    access_modes          = var.storage_access
    storage_class_name    = "yc-s3"
    persistent_volume_source {
      csi {
        driver            = "ru.yandex.s3.csi"
        volume_handle     = var.bucket_minio_name
        volume_attributes = {
          capacity        = var.storage_capacity
          mounter         = "geesefs"
        }
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "minio_pvc" {
  metadata {
    name = "minio-pvc"
  }
  spec {
    resources {
      requests  = {
        storage = var.storage_capacity
      }
    }
    access_modes       = var.storage_access
    storage_class_name = ""
  }
}

# create minio replicated pods
resource "kubernetes_replication_controller" "minio_rc" {
  metadata {
    name          = "minio-rc"
    labels        = {
      application = "minio"
    }
  }
  spec {
    replicas      = var.minio_replicas_num
    selector      = {
      application = "minio"
    }
    template {
      metadata {
        name          = "minio-pod"
        labels        = {
          application = "minio"
        }
      }
      spec {
        container {
          name  = "minio-container"
          image = "minio/minio"
          port {
            container_port = var.minio_port
          }
          volume_mount {
            name       = var.minio_volume_name
            mount_path = var.minio_volume_mount_path
          }
          env {
            name = "MINIO_SERVER_URL"
            value = var.minio_url
          }
          command = ["/bin/sh", "-c"]
          args    = ["minio server --console-address :${var.minio_port} /data"]
        }
        volume {
          name = "minio-data"
          persistent_volume_claim {
            claim_name = "minio-pvc"
          }
        }
      }
    }
  }
}

# create service for a minio pods
resource "kubernetes_service" "minio_service" {
  metadata {
    name = "minio-service"
  }
  spec {
    selector      = {
      application = "${kubernetes_replication_controller.minio_rc.metadata.0.labels.application}"
    }
    port {
      port        = var.minio_port
      target_port = var.minio_port
    }
    type          = "LoadBalancer"
  }
}
