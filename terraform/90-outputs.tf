output "loadbalancer_ip" {
  value = "${kubernetes_service.minio_service.status.0.load_balancer.0.ingress.0.ip}"
}

output "k8s_external_ip" {
  value = yandex_kubernetes_cluster.k8s-cluster.master.0.external_v4_address
}
