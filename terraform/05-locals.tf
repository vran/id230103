# create local kubeconfig file at manage pc for a created kubernetes cluster
locals {
  kubeconfig = <<KUBECONFIG
apiVersion: ${var.locals_api_version}
clusters:
- cluster:
    server: ${yandex_kubernetes_cluster.k8s-cluster.master[0].external_v4_endpoint}
    certificate-authority-data: ${base64encode(yandex_kubernetes_cluster.k8s-cluster.master[0].cluster_ca_certificate)}
  name: ${val.locals_cluster_name}
contexts:
- context:
    cluster: ${val.locals_cluster_name}
    user: ${var.locals_user}
  name: ${var.locals_context_name}
current-context: ${var.locals_context_name}
users:
- name: ${var.locals_user}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: yc
      args:
      - k8s
      - create-token
KUBECONFIG
}

resource "local_file" "kubeconfig" {
    content  = local.kubeconfig
    filename = var.kubeconfig_file
}
