MinIO in Docker Swarm and/or Kubernetes
=========
Ansible playbook to deploy minio in docker swarm  
Terraform config files to deploy minio in Kubernetess cluster at Yandex Cloud  

Requirements
------------
**For ansible playbook:**  
  
* Hosts OS:
    * Debian Bullseye 11 (stable)  
    * Debian Buster 10 (oldstable)  
    * Raspbian Bullseye 11 (stable)  
    * Raspbian Buster 10 (oldstable)  
  
* Soft:
    * ansible >= 2.14  
    * python3-pip  
    * rsync  
    * curl  
    * git  
    * molecule  
    * molecule-docker  

Playbook uses these my roles:  
* Docker: https://gitlab.com/ansible_roles_v/docker.git  
* Swarm cluster: https://gitlab.com/ansible_roles_v/swarm_cluster.git  
* MinIO in swarm cluster: https://gitlab.com/ansible_roles_v/minio_in_swarm.git  

**For minio in kubernetes:**  
  
* Terraform installed  
* Yandex Cloud account registered, created cloud and folder for project  
